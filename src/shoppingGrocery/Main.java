// Grocery Shopping
//
//
// Create class Product, it should contain at least two fields – name and price.
// Create an empty array of Products – it’s size should be at least 5.
// Fill it within while loop. Simulate the process of doing shopping:
//
// - ask for a product,
// - add it to the cart (array),
// - change index,
// - if index will be greater than 5 – finish shopping,
// - *pay for the products.
package shoppingGrocery;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // int[] arr = new int[5]; = wrong
        Product[] arrayProducts = new Product[5];
        // Product product1 = new Product("plus = ", 5);
        // arrayProducts[0] = product1;
        arrayProducts[0] = new Product("Water = ", 5);
        arrayProducts[1] = new Product("Juice = ", 10);
        arrayProducts[2] = new Product("Chips = ", 8);
        arrayProducts[3] = new Product("Candy's = ", 3);
        arrayProducts[4] = new Product("Gum = ", 2);
        showMenu(arrayProducts);

        Product[] cart = addToCart(arrayProducts); // adding products to the cart
        System.out.println("Cart selected Products: ");
        displayCart(cart);
        System.out.println(cartTotalPrice(cart));
        int totalPrice = cartTotalPrice(cart);
        payCart(totalPrice);

    }

    public static void showMenu(Product[] products) {
        System.out.println("Type the number of the product: \n");

        for (int i = 0; i < products.length; i++) {
            System.out.println("[" + (i) + "]" + " for: " + products[i].show() + " Ron");
        }
    }

    public static void displayCart(Product[] cart) {

        for (int i = 0; i < cart.length; i++) {
            System.out.println("Selected Product = " + cart[i].show());
        }
    }

    public static int cartTotalPrice(Product[] cart) {
        int sum = 0;
        for (int i = 0; i < cart.length; i++) {
            sum += cart[i].getPrice();
        }
        System.out.println("Insert total: ");
        return sum;
    }

    /**
     * Adding products in the basket and make the sum of the cart
     *
     * @return the sum of the products in the cart
     */

    public static Product[] addToCart(Product[] arrayProduct) {
        Scanner scanner = new Scanner(System.in);
        Product[] cart = new Product[5];

        int i = 0;
        int option = 0;
        while (i < cart.length) {
            option = scanner.nextInt();
            if (option < 0 || option >= arrayProduct.length) {
                System.out.println("Invalid Option !");
                continue; // used to return back to while if i is invalid !
                // BREAK = used to stop looping
            }
            cart[i] = arrayProduct[option];
            i++;
        }
        return cart;
    }

    /**
     * Requesting cartPrice total from user
     *
     * @param cartTotalPrice amount to be paid by user
     */

    public static void payCart(int cartTotalPrice) {
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        while (userInput != cartTotalPrice) {
            System.out.println("Please Insert " + cartTotalPrice);
            userInput = scanner.nextInt();
        }
        System.out.println("All products are Paid! Thank you !");
    }
}
