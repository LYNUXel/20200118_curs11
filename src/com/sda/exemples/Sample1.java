package com.sda.exemples;

public class Sample1 {
    public static void main(String[] args) { // String [] = Constructori
        String a = new String("Start"); // = constructor ce primeste un parametru
        // a este o variabila, dupa egal am apelat clasa cosntructorului, String este tipul variabilei, a contine
        a.toLowerCase(); // a. apar metode
        // avem tipuri de variabile (primitive si complexe),
        // - pot fi locale (numai in metode), sau
        // - loop variables (folosite in while sau for)
        // - variabila de referinta
        // - instance variable
        System.out.println(a.toLowerCase());
        Integer n = new Integer(2); // in dreapta este constructor, in () parametri constructorului = > n este variabila de referinta, care tine un obiesct de tipul integer.
    }
}
