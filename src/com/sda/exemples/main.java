package com.sda.exemples;

public class main {
    public static void main(String[] args) {
        // need a object => create an object "Student"
        Student student1 = new Student("George", "123156455464", 32);
        student1.study();
        student1.displayStudent();
        System.out.println(student1);
        Student student2 = new Student("Alina","21156546564", 23);
        student2.study(); // method behaviour !
        student1.displayStudent();
        student2.displayStudent();
        Student.staticMethod(); // call the method from Student.java
        // main a = new main (); <= created this object to call displayMessagge !
        displayMessage(); // displayMessage shown only with public static void displayMessage !
    }

    public static void displayMessage() {
        System.out.println("staticMethod inserted, Message: Hello World");
    }
}
