package com.sda.exemples;

/**
 * Class used for modelling a student.
 */

public class Student {
    // state
    private String name; // class members => must be PRIVATE !!!
    private String CNP;
    private int age;


    /**
     * behaviour
     * Constructor with parameters
     * @param n - name of the student
     * @param C - CNP of the student
     * @param a - age of the student
     */

    public Student(String n, String C, int a) { // constructor = used for initializate
        name = n;
        CNP = C;
        age = a;
    }

    /**
     * Method's used for making student study
     */

    public void study() {
        System.out.println(name + " is studying");
        // in a class, we have first the state and then the behaviour
    }

    public void displayStudent() {
        System.out.println("Student name is: " + name + " and CNP is: " + CNP);
        System.out.println(" Student age is: " + age);
        staticMethod(); //Show the message
    }

    public static void staticMethod() { // could be accessed upper too in "public"
        System.out.println("Hello from static method!");
        // displayStudent (); ERROR: can't be accessed from outside of the variable

    }
}
